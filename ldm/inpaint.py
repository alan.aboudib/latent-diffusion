import argparse, os, sys, glob
from omegaconf import OmegaConf
from PIL import Image
from tqdm import tqdm
import numpy as np
import torch
import gdown
import pkg_resources

from .models.diffusion.ddim import DDIMSampler
from .main import instantiate_from_config



def make_batch(image, mask, device):
    image = np.array(Image.open(image).convert("RGB"))
    image = image.astype(np.float32)/255.0
    image = image[None].transpose(0,3,1,2)
    image = torch.from_numpy(image)

    mask = np.array(Image.open(mask).convert("L"))
    mask = mask.astype(np.float32)/255.0
    mask = mask[None,None]
    mask[mask < 0.5] = 0
    mask[mask >= 0.5] = 1
    mask = torch.from_numpy(mask)

    masked_image = (1-mask)*image

    batch = {"image": image, "mask": mask, "masked_image": masked_image}
    for k in batch:
        batch[k] = batch[k].to(device=device)
        batch[k] = batch[k]*2.0-1.0
    return batch

def download_checkpoint():
    """Download model checkpoint.
    """
    
    # Download official weights
    if not os.path.exists("./ldm_saved_models"):

        print("Downloading the LDM model's checkpoint ...")
        
        os.mkdir('./ldm_saved_models')

        MODEL_PATH_URL = "https://heibox.uni-heidelberg.de/f/4d9ac7ea40c64582b7c9/?dl=1"

        gdown.download(MODEL_PATH_URL, "./ldm_saved_models/last.ckpt", use_cookies=False)

    print("LDM model's checkkpoint saved.")

    
def inpaint_image(indir, outdir, steps):
    """Perform image inpainting

    Args:
       indir (str): dir containing image-mask pairs (`example.png` and `example_mask.png`)
       outdir (str): dir to write results to
       steps (int): number of ddim sampling steps
    """

    masks = sorted(glob.glob(os.path.join(indir, "*_mask.png")))
    images = [x.replace("_mask.png", ".png") for x in masks]
    print(f"Found {len(masks)} inputs.")

    # Load the config file
    config_file_path = pkg_resources.resource_filename('ldm', 'configs/models/ldm/inpainting_big/config.yaml')
    config = OmegaConf.load(config_file_path)
    model = instantiate_from_config(config.model)


    download_checkpoint()
    
    model.load_state_dict(torch.load("./ldm_saved_models/last.ckpt")["state_dict"],
                          strict=False)

    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    model = model.to(device)
    sampler = DDIMSampler(model)

    os.makedirs(outdir, exist_ok=True)
    with torch.no_grad():
        with model.ema_scope():
            for image, mask in tqdm(zip(images, masks)):
                outpath = os.path.join(outdir, os.path.split(image)[1])
                batch = make_batch(image, mask, device=device)

                # encode masked image and concat downsampled mask
                c = model.cond_stage_model.encode(batch["masked_image"])
                cc = torch.nn.functional.interpolate(batch["mask"],
                                                     size=c.shape[-2:])
                c = torch.cat((c, cc), dim=1)

                shape = (c.shape[1]-1,)+c.shape[2:]
                samples_ddim, _ = sampler.sample(S=steps,
                                                 conditioning=c,
                                                 batch_size=c.shape[0],
                                                 shape=shape,
                                                 verbose=False)
                x_samples_ddim = model.decode_first_stage(samples_ddim)

                image = torch.clamp((batch["image"]+1.0)/2.0,
                                    min=0.0, max=1.0)
                mask = torch.clamp((batch["mask"]+1.0)/2.0,
                                   min=0.0, max=1.0)
                predicted_image = torch.clamp((x_samples_ddim+1.0)/2.0,
                                              min=0.0, max=1.0)

                inpainted = (1-mask)*image+mask*predicted_image
                inpainted = inpainted.cpu().numpy().transpose(0,2,3,1)[0]*255

                inpainted = Image.fromarray(inpainted.astype(np.uint8))
                inpainted.save(outpath)

                print()
                print(f"Inpainted image saved to {outpath}")
                print()
                
    return inpainted
