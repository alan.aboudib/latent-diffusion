from setuptools import setup, find_packages

setup(
    name='latent-diffusion',
    version='0.0.1',
    description='',
    packages=find_packages(),
    install_requires=[
        'torch==1.13.1',
        'numpy',
        'tqdm',
        'omegaconf',
        'pytorch-lightning==1.6.5',
        'torch-fidelity',
        'einops',
        'taming-transformers-rom1504',
        'gdown'
    ],
    include_package_data=True,

)
